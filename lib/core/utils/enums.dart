enum RequestState{
  loading,
  loaded,
  error
}
enum ThemeApp{
  light,
  dark,
  system
}
enum StateDomain{
  start,
  luck,
  complete,
  skipped,
}
enum StateImage{
  local,
  remote,
}
enum BottomSheetOption{
  profile,
  empty,
}
