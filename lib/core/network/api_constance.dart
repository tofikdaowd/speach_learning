class ApiConstance{
  static const String baseUrl = "http://voca.najeeb-edu.sy/api";
  static const String apiParticipantKey = "$baseUrl/participants";

  static const String getParticipantWithId = "$apiParticipantKey/by-id";
  // static const String allSectionsPath = "$baseUrl/ ?api_key=$apiKey";
  // static const String participantsDomainPath = "$baseUrl/ ?api_key=$apiKey";
  // static const String participantPath = "$baseUrl/ ?api_key=$apiKey";

}